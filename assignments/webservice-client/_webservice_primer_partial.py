import requests
import json

class _webservice_primer:
    def __init__(self): # constructor, self is imp
        self.SITE_URL = 'http://student04.cse.nd.edu:51001'
        self.MOVIES_URL = self.SITE_URL + '/movies/'
        self.RESET_URL = self.SITE_URL + '/reset/'

    def get_movie(self, mid):
        r = requests.get(self.MOVIES_URL + str(mid))
        resp = json.loads(r.content)
        return(resp)

    def reset_movie(self, mid):
        movie_data = {} # empty dictionary, to pass in request body
        movie_string = json.dumps(movie_data)
        r = requests.put(self.RESET_URL + str(mid), data=movie_string) #a request with with a message body as data
        resp = json.loads(r.content.decode('utf-8'))
        return(resp)

    def set_movie_title(self, mid, new_title):
        '''makes a put request to /movies/mid with body whole movie json as string with new title'''
        # creating body of message
        movie_json = self.get_movie(mid) 
        movie_json['title'] = new_title
        movie_string = json.dumps(movie_json)
        # making the put request
        r = requests.put(self.MOVIES_URL + str(mid), data = movie_string)
        response_json = json.loads(r.content)
        return response_json

    def delete_movie(self, mid):
        '''issues a delete request to /movies/mid with a body '{}'''
        empty_dictionary = {}
        message = json.dumps(empty_dictionary)
        r = requests.delete(self.MOVIES_URL + str(mid), data = message)
        resp_json = json.loads(r.content)
        return resp_json
    


    def display_movie(self, mid):
        movie = self.get_movie(mid)
        if movie['result'] == 'success':
            print("Title: " + movie['title'])
        else:
            print("Error: " + movie['message'])

if __name__ == "__main__":
    MID = XX # change this to your assigne MID
    ws = _webservice_primer()
    print(ws.get_movie(MID))
    ws.display_movie(MID)
