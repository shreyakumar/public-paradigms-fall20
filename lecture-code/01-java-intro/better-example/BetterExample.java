// Better example using inheritance
import java.util.*;

class Person{
    // properties
    String netid;
    String firstName;
    String lastName;
    Date joiningDate;
    String classification;

    // behavior

    String findFood(){
        return "go to Duncan Center";
    }



} // end of person

class Student extends Person{ // Student is a child class of Person(parent)
    double gpa;    

    double calculateFinancialAid(){
        return 500000;
    }

} // end of Student class

class Faculty extends Person{

    double calculateSalary(){
       return 100000;
    }

} // end of Faculty class

public class BetterExample{

    public static void main(String[] args){
        Student s = new Student();
        Faculty f = new Faculty();

        System.out.println("Salary: " + f.calculateSalary());
        System.out.println("Fin Aid: " + s.calculateFinancialAid());
        System.out.println("Food: " + s.findFood());
        
        // array of Persons filled with Faculty and Students 
        Person[] p_arr = new Person[3];
        p_arr[0] = new Student(); // directly through object

        Faculty fp = new Faculty();
        p_arr[1] = fp; // with a variable pointing to an obj

	Person ps = new Student(); // note mismatch types, allowed
	p_arr[2] = ps;

	for (Person x : p_arr){ 
		// we can perform some loop actions
		// that we know will work on f and s
		System.out.println(x.findFood());
	}
    } // end of main

}
