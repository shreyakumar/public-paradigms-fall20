package main

import (
    "fmt"
    "bufio"
    "net/http"
)

func main() {
    fmt.Println("Hello, playground")
    urlString := "https://api.agify.io/?name=" + "Bagheera"
    
    resp, err := http.Get(urlString) // makes the request

    if err != nil {
        fmt.Println("connection error: ", err)
    }
    defer resp.Body.Close()

    //Print the HTTP response status.
    fmt.Println("Response status:", resp.Status)
    //Print the first 5 lines of the response body.
    scanner := bufio.NewScanner(resp.Body)
    if scanner.Scan() {
        fmt.Println(scanner.Text())
    }
    
    if err := scanner.Err(); err != nil {
        fmt.Println("scanner error: ", err)
    }
} // end of main
